class AttachmentsController < BaseController
  skip_before_filter :authenticate_user!

  def create
    #@attachment = Attachment.new(:file)
    #@attachment = Attachment.create( attachment_params )
    attachment = Attachment.create(permitted_params.attachment)
    #attachment = Attachment.new(params[:file])
    #@attachment = params[:attachment]
    attachment.user = current_user
    attachment.filename = "test.jpg"
    attachment.location = "/public/uploads"

    respond_to do |format|
      if attachment.save
        Measurement.increment('attachments.create.success')
        #render json: { attachment_id: "asdasdasd" }
        #files: [attachment.to_jq_upload],
        format.json { render json: {files: [attachment.to_jq_upload],saved: true, attachment_id: @ttachment.id} }
      else
        Measurement.increment('attachments.create.error')
        format.json { render json: attachment.errors, status: :unprocessable_entity }
      end
    end
  end

  def iframe_upload_result
    render layout: false
  end

  private
  def attachment_params
   #params.require(:attachment).permit!
    #params.permit(:user,:filename,:location,:filesize,"utf8","authenticity_token","fileuploader")
    #permitted_params.attachment
    params.require(:file).permit(:filename, :location, :filesize, :redirect, :key, :utf8,:user,:fileupload,ActionDispatch::Http::UploadedFile)
  end

end