class Attachment < ActiveRecord::Base

  belongs_to :user
  belongs_to :comment, counter_cache: true
  validates_presence_of :filename, :location, :user_id
  before_destroy :delete_from_storage

  has_attached_file :file
  validates_attachment_content_type :file, :content_type => ['image/png', 'image/gif', 'image/jpg']

  include Rails.application.routes.url_helpers
  def to_jq_upload
    {
        "name" => read_attribute(:attachments_file_name),
        "size" => read_attribute(:attachments_file_size),
        "location" => "/home/loomio/projects/loomio"
    }
  end


  def is_an_image?
    %w[jpg jpeg png gif].include?(filetype)
  end

  def truncated_filename(length = 30)
    if filename.length > length
      filename.truncate(length) + filetype
    else
      filename
    end
  end

  def filetype
    filename.split('.').last.downcase
  end

end

