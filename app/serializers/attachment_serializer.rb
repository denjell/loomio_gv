class AttachmentSerializer < ActiveModel::Serializer
  embed :ids, include: true
  attributes :id, :filename, :location, :filesize

  has_one :comment, serializer: CommentSerializer

end
